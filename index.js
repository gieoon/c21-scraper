//local imports
const seek = require('./Seek.com/index.js');

//library imports
const puppeteer = require('puppeteer');
var browser = null;
var count = 0;

async function run(close){

	console.log("LAUNCHING");

	const width = 1400;
	const height = 1600;

	const browser = await puppeteer.launch({
        headless: false,
        args: [
            `--window-size=${ width },${ height }`
        ],
    });

	const page = await browser.newPage();

	await page.setViewport({ width, height });

	seek.go(page, close);

}

async function close() {
	await browser.close();
}

run(function(){
	//close();
});
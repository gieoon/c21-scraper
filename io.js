// const jsonexport = require('jsonexport');
 const fs = require('fs');

// var reader = null;
// var writer = null;

const csvwriter = require('csv-write-stream');
const writer = csvwriter();

exports.openStream = function(){
	// const jsonexport = require('jsonexport');
	// const fs = require('fs');

	// reader = fs.createReadStream('in.json');
	// writer = fs.createWriteStream(Date.now().toString() + '.csv');

	// reader.pipe(jsonexport()).pipe(writer);
	const currentDate = getDate();
	var writerStream = fs.createWriteStream('./data/' + currentDate + '.csv');
	// writerStream.on('finish', (fd) => {
	// 	consolelog('write completed');
	// });
	//console.log('piping writer');
	writer.pipe(writerStream);
	

}

function getDate(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();


	if(dd<10) {
	    dd = '0'+dd
	} 

	if(mm<10) {
	    mm = '0'+mm
	} 

	//return mm + '-' + dd + '-' + yyyy + ':' + Date.now().toString();
	return Date.now().toString();
}

exports.closeStream = function(){
	writer.end();
}

exports.saveToFile = function(data){
	console.log("-----------------------------------");
	console.log("WRITING TO FILE...");

	//var Json2csvParser = require('json2csv').Parser;
	//const jsonexport = require('jsonexport');
	const fs = require('fs');

	// let fields = [
	// 	'Title', 
	// 	'Company', 
	// 	'Location', 
	// 	'Requesting Chris21', 
	// 	'Requesting MYOB',
	// 	'Requesting EMPLive',
	// 	'Requesting ADP',
	// 	'RoleType',
	// 	'Url'
	// ];

	const contents = {
		'Title': 				data.title, 
		'Company': 				data.company,
		'Location': 			data.location,
		'RoleType': 			data.roleType,
		'Chris21': 				data.switches[0] ? 'Wants Chris21' : '',
		'MYOB': 				data.switches[1] ? 'Wants ADP' : '',
		'EMPLive': 				data.switches[2] ? 'Wants MYOB' : '',
		'ADP': 					data.switches[3] ? 'Wants EMPLive' : '',
		'Url': 					data.pageUrl
	};
	writer.write(contents);


	//const json2csvParser = new Json2csvParser({ fields });
	
	// jsonexport(
	// 	contents,
	// 	function(err, csv){
	// 		if(err) console.log(err);
	// 		reader.write(Date.now().toString() + '.csv', csv, function(err) {
	// 			if(err) throw err;
	// 			console.log('FILE SAVED');
	// 			console.log("-----------------------------------");
	// 		});
	// 	}
	// );

	// const input = fs.createReadStream('/data', { encoding: 'utf8' });
	// const output = fs.createWriteStream('/data', { encoding: 'utf8' });
	// const json2csvParser = new Json2csvParser({ fields });
	// //const csv = json2csvParser.parse(data);
	// const processor = input.pipe(json2csv).pipe(output);
	//console.log("WRITING FILE COMPLETED SUCCESSFULLY");
	//console.log("-----------------------------------");

	// fs.writeFile("/data/" + data.startTime, data.string, (err) => {
	// 	if(err){
	// 		return console.log("ERROR WRITING FILE: " + err);
	// 	}

	
	// });
}

const io = require('../io');

const URL = 'https://www.seek.com.au/';
const searchStrings = [
	'chris21',
	'myob',
	'emplive',
	'adb'
];
const startTime = Date.now().toString();
let subJobCount = 0;
let subCategoryText = '';
var countList = {};
var currentPageUrl = '';
var pageCount = 1;
var jobListingCount = 1;

/*
	This scraper will do the following:
	1.Click on middle dropdown input
	2.Select Accounting
	3.Select Payroll
	4.Submit search
	5.Go into page, gather results
	6.Save results in file
	7.Repeat and go to next page when necessary.
*/


exports.go = async function(page, close){

	io.openStream();
	
	initCount();
	await page.goto(URL);

	const title = await page.title();
	console.log("title: " + title);
	
	//click dropdown label
	const dropdown = await page.waitForSelector('#SearchBar__Classifications');

	await dropdown.click();

	const dropdownDiv =  await page.waitForSelector('._3cvRF2q > span > span');
	var dropLi = await page.$$('li[data-automation="item-depth-0"]');//'._3cvRF2q > span > span');
	
	const ACCOUNTING_INDEX = 0;
	const PAYROLL_INDEX = 20;
	let category = await page.$$('._3cvRF2q > span > span');
	var categoryText = await category[ACCOUNTING_INDEX].getProperty('innerText');
	categoryText = await categoryText.jsonValue();
	page.waitFor(500);
	console.log("umbrella category: " + categoryText);
	const bigCategory = categoryText;

	let jobCount = await page.$$('._3v2SJ91 > span > span');
	jobCount = await jobCount[ACCOUNTING_INDEX].getProperty('innerText');
	jobCount = await jobCount.jsonValue();
	console.log('jobs total: ' + jobCount);
	const totalJobCount = jobCount;

	//subcategory list
	await category[ACCOUNTING_INDEX].click();
	await page.waitForSelector('li[data-automation="item-depth-1"]');//'._3cvRF2q > ul > li > ._3cvRF2q');
	
	const subCategories = await page.$$('li[data-automation="item-depth-1"]');

	subCategoryText = await page.$$('li[data-automation="item-depth-1"] > a > span'); //'._3cvRF2q > ul > li > ._3cvRF2q > span > span'
	subCategoryText = await subCategoryText[PAYROLL_INDEX].getProperty('innerText');
	subCategoryText = await subCategoryText.jsonValue();
	console.log("subcategory: " + subCategoryText);

	subJobCount = await page.$$('li[data-automation="item-depth-1"] > span > span');//'._3cvRF2q > ul > li > ._3cvRF2q > span:nth-child(2) > span'
	subJobCount = await subJobCount[PAYROLL_INDEX].getProperty('innerText');
	subJobCount = await subJobCount.jsonValue();
	console.log("sub job: " + subJobCount); 

	await subCategories[PAYROLL_INDEX].click();

	const submitButton = await page.waitForSelector('button[type="submit"]');
	
	await submitButton.click();
	await page.waitFor(1500);
	await submitButton.click();


//----------------------------------------------------------------------------------------//

	//look at results page
	await page.waitForSelector('._1EkZJQ7');//'a[data-automation="jobTitle"]');
	
	const jobArticles = await page.$$('._1EkZJQ7');
	console.log('jobArticles: ', jobArticles.length);

	await checkPageResults(page, 0, jobArticles.length, 1);

}

async function goNextPage(page, pageNo){
	++pageNo;
	try{
		console.log('=> GOING TO NEXT PAGE =>');
		//wait for 'next' button to appear
		await page.waitForSelector('a[data-automation="page-next"]');
		await page.goto(URL + 'jobs-in-accounting/payroll?page=' + pageNo);
		console.log('0.5');
		await page.waitFor(3000);
		//await page.waitForNavigation({waitUntil: "networkidle0"});
		console.log('1');
		await page.waitForSelector('._1EkZJQ7');//'a[data-automation="jobTitle"]');
		console.log('2');
		const jobArticles = await page.$$('._1EkZJQ7');

		console.log('jobArticles: ', jobArticles.length);
		await checkPageResults(page, 0, jobArticles.length, pageNo);

	}
	catch(err){
		console.log('NO MORE PAGES, EXITING: ', pageNo);
		end();
		return;
	}
}

function end(){
	io.closeStream();
	
	const endTime = Date.now().toString();

	const durationTime = endTime - startTime;
	console.log('-----------------------------------------------');
	console.log('FINISHED PROCESS SUCCESSFULLY');
	console.log('Jobs Queried: ' + subJobCount);
	console.log('In Category: '+ subCategoryText);
	console.log('Duration: ' + durationTime);
	console.log('-----------------------------------------------');
}

async function checkPageResults(page, jobTitleIndex, totalCount, pageNo){
	//console.log('checkPageResults()');
	await page.waitForSelector('a[data-automation="jobTitle"]');
	currentPageUrl = await page.evaluate(() => window.location.href);
	//console.log('1');
	await page.waitForSelector('a[data-automation="jobTitle"]');
	const jobResults = await page.$$('a[data-automation="jobTitle"]'); //._1EkZJQ7

	console.log('jobresults.length: ', jobResults.length);
	console.log('index: ', jobTitleIndex);
	try{
		//sometimes the number of listings changes, so if error need to move on to next page
		await jobResults[jobTitleIndex].click();
	}catch(err){
		goNextPage(page, pageNo);
	}
	
	//console.log('2');
	try{
		await page.waitForNavigation({waitUntil: "networkidle0"});
		await checkPage(page, jobTitleIndex, jobResults.length, pageNo);
	} catch(err){
		//if timeout, continue as normal, most likely website has loaded
		await checkPage(page, jobTitleIndex, jobResults.length, pageNo);
	}
	return;
}

async function checkPage(page, jobTitleIndex, totalCount, pageNo){
	//try{
		//console.log("checkPage()");
		await page.waitForSelector('div.JyFVSRZ');
		//console.log("current url: ", currentPageUrl);
		const title = await page.$eval('span[data-automation="job-detail-title"] > span > h2', e => e.innerHTML);
		console.log('title: ', title);
		
		
		//for(var i = 0; i < searchStrings.length; i++) {
			//const foundString = (await page.content()).match('/\b' + searchTerm + '\b/');
			// var foundString = ''; 
			// //difficult to plug variables into regex...
			// switch (i){
			// 	case 0 : 
			// 		foundString = (await page.content()).match(/[^\w\s]chris21[^\w\s]/gi);
			// 		break;
			// 	case 1 :
			// 		foundString = (await page.content()).match(/myob/gi);
			// 		break;
			// 	case 2 : 
			// 		foundString = (await page.content()).match(/emplive/gi);
			// 		break;
			// 	case 3 :
			// 		foundString = (await page.content()).match(/[^\w\s]adp[^\w\s]/gi);
			// 		break;
			// }

			var switches = await checkStringForText(page);
			
			//console.log(searchStrings[i], ' : ', foundString);
			//if(foundString !== null){
				//switches[i] = true;
				//console.log(searchStrings[i] + ' found');
				//countList[searchStrings[i]].frequency++;
				//console.log(countList[searchStrings[i]].title, ': ', countList[searchStrings[i]].frequency);
			//} else {
			//	switches[i] = false;
			//}
			//console.log(switches);
		//};
		//save to file
		await getPageDetails(page, title, switches);
		//revert to previous page
		await page.goto(currentPageUrl);
		++jobTitleIndex;
		if(jobTitleIndex >= totalCount){
			//go to next page
			goNextPage(page, pageNo);
		}
		else{
			checkPageResults(page, jobTitleIndex, totalCount, pageNo);
		}
		return;
}


async function checkStringForText(page){
	var switches = [false, false, false, false];
	const text = await page.$eval('*', el => el.innerText); 
	//console.log('text: ', text);
	switches[0] = text.search(/Chris21/i) > 0;
	switches[1] = text.search(/ADP/i) > 0;
	switches[2] = text.search(/MYOB/i) > 0;
	switches[3] = text.search(/EMPLive/i) > 0;
	return switches;
}

async function getPageDetails(page, title, switches){
	//console.log('5');
 	//await page.waitForSelector('span[data-automation="job-header-company-review-title"] > span');
	//small screen
	//const company = await page.$eval('span[data-automation="job-header-company-review-title"] > span', e => e.innerHTML);
	//big screen

	var company = '';
	try{
		company = await page.$eval('span[data-automation="advertiser-name"] > span', e => e.innerHTML);
	}catch(err){
		console.log('element not found, trying other');
		try{
			company = await page.$eval('span[data-automation="job-header-company-review-title"] > span', e => e.innerHTML);
		}
		catch(err){
			company = 'N/A';
		}
	}
	
	console.log('company: ', company);

	const pageUrl = await page.evaluate(() => window.location.href); 
	console.log('pageUrl: ', pageUrl);

	const location = await page.$eval('dd > span > span > strong', e => e.innerHTML); //:nth-child(1)
	console.log('location: ', location);

	const roleType = await page.$eval('dd[data-automation="job-detail-work-type"] > span > span', e => e.innerHTML);
	console.log('roleType: ', roleType);

	io.saveToFile({
		title: title, 
		company: company,
		location: location,
		pageUrl: pageUrl,
		switches: switches,
		roleType: roleType
	});

	return;
}

function initCount(){
	searchStrings.forEach(searchTerm => {
		countList[searchTerm] = {
			title: searchTerm,
			frequency: 0
		};
	});
	//console.log(countList);
}
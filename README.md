## Seek Scraper

Scrapes one page to another

to run: '$ node index.js'
regular debugging console commands.
Currently, the program does not close without adjustment, but methods are in place.

The scrape rate is necessarily slow, a fast request rate would not imitate human interaction, and would be inconsiderate.

# Output
CSV Files are output to /data directory named by the timestamp of when they were created

# Issues
At least in one instance, ADP was not picked up, despitebeing blatantly in the title,
A Ctrl + F on the webpage showed that it was there 7 times. This is an issue that needs to be looked into.



